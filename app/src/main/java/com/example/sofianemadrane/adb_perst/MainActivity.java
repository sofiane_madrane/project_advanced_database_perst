package com.example.sofianemadrane.adb_perst;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintStream;

public class MainActivity extends AppCompatActivity {

    private TextView tv ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        this.tv = (TextView) findViewById(R.id.monTextview);
    }

    public void click_sqlite(View view) {
        CharSequence s = "Click SqLite";
        Toast.makeText(this, s, Toast.LENGTH_SHORT).show();
        benchmark("sqlite");

    }

    public void click_perst(View view) {
        CharSequence s = "Click Perst";
        Toast.makeText(this, s, Toast.LENGTH_SHORT).show();
        benchmark("perst");
    }

    public void benchmark(String actionDB){
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        String databasePath = "testindex.dbs";
        try {
            // It is necessary to create file using Activity.openFileUutput method otherwise
            // java.io.RandomAccessFile will not able to open it (even in write mode). It seems to be a bug
            // which I expect to be fixed in next release of Android, but right now this two lines fix
            // this problem.
            this.openFileOutput(databasePath, 0).close();
            databasePath = getFileStreamPath(databasePath).getAbsolutePath();
        } catch (IOException x) {}
        PrintStream ps = new PrintStream(out);
        Test test;
        //switch (item.getId()) {
        if(actionDB.equals("perst")){
            test = new PerstTest(databasePath, ps);
        }else{
            test = new SqlLiteTest(databasePath, ps);
        }
        new Thread(test).start();
        new Thread(new ProgressMonitor(test, tv, out)).start();
    }
}
