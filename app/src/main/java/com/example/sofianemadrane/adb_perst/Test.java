package com.example.sofianemadrane.adb_perst;

/**
 * Created by sofianemadrane on 5/12/17.
 */


public abstract class Test implements Runnable {
    boolean completed;

    public synchronized void done() {
        completed = true;
        notify();
    }

    public abstract String getName();
}
